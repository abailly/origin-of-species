(import scheme
        srfi-133
        generic-helpers
        format
        (chicken io)
        (chicken random))

(define (mutate-biome-at! biome pos)
  (let* ((factor (vector-ref biome pos))
         (new-factor (case (pseudo-random-integer 3)
                       ((0) 0)
                       ((1) 1)
                       ((2) '?))))
    (begin
      (vector-set! biome pos new-factor)
      biome)))

;;; The percentage of chance a biotope is 'mutated'
(define-constant *biotope-change-rate* 30)

;;; Mutate a biotope
;;; A `biotope` is a pair of a vector of ternary values, called a `biome` and a supported population number.
;;; the mutation can change both the `biome` and the population number.
(define (mutate-biotope biotope)
  (let* ((len (vector-length (car biotope)))
         (pop (cdr biotope))
         ;;; biome has a 50% chance of being mutated
         (mut-biome (pseudo-random-integer 100))
         (new-biome (vector-copy (car biotope))))
    (if (< mut-biome *biotope-change-rate*)
        (cons (mutate-biome-at! new-biome (pseudo-random-integer len)) pop)
        biotope
        )))

(define (color-of-biotope acc x)
  (if (number? x)
      (+ (* 2 acc) x)
      (+ 1 acc)))

;;; Returns a string with ANSI-escape color sequences to
;;; represent the given biotope.
;;; `biotope` is a list whose first element describes the terrain and subsequent
;;; elements, if any, the genomes populating this terrain.
;;;
;;; the main color is given by the `biome` sequence, eg. the sequence of ternary
;;; values describing the location's capabilities to support life.
;;; On top of it we add the number of species which are present in this biotope.
(define (show-biotope biotope)
  (let ((color-code (vector-fold-right color-of-biotope
                                       0
                                       (caar biotope)))
        (count (length (cdr biotope))))
    (format "\x1b[48;5;~Am~3D\x1b[0m" color-code count)))

(define (print-map-with a-map port)
  (let* ((print-biotope (lambda (biotope)
                          (write-string (show-biotope biotope) #f port)))
         (print-row (lambda (row)
                         (begin
                           (vector-map print-biotope row)
                           (newline port)))))
    (vector-map print-row a-map)))

;;; Apply a function `f` on each cell of the `maps` yielding a new map.
(define (map-map f . maps)
  (apply vector-map
         (cons (lambda (row . rows)
                 (apply vector-map (cons f (cons row rows))))
               maps)))

;;; Fold a function `f` on each cell of the `maps`.
;;; function `f` is given as first 2 arguments the coordinates `(x,y)` of the
;;; cell on the map, the accumulator as third argument, and then one argument
;;; per map iterated upon.
;;; Asumes all maps have the same size.
(define (fold-map-with-index f accumulator . maps)
  (let* ((first-map (car maps))
         (row-nums (vector-unfold (lambda (i _)
                                    (values i i))
                                  (vector-length first-map)
                                  0))
         (column-nums (vector-unfold (lambda (i _)
                                       (values i i))
                                     (vector-length (vector-ref first-map 0))
                                     0)))
    (apply vector-fold
           (cons (lambda (acc i row . rows)
                   (apply vector-fold
                          (cons (lambda (row-acc j . args)
                                  (apply f (append (list  i j row-acc) args)))
                                (cons acc
                                      (cons column-nums (cons row rows))))))
                 (cons accumulator
                       (cons row-nums maps))))))

(define (biotope-at i j biotopes)
  (if (or (<= (vector-length biotopes) i)
          (<= (vector-length (vector-ref biotopes 0)) j))
      '()
      (vector-ref
       (vector-ref biotopes i)
       j)))

;;; Print a `map`
;;; The map is printed using colors through ANSI escape sequences hence
;;; requires proper support from the underlying port, eg. it should be
;;; a term
(define print-map
  (case-lambda
    ((a-map)
     (print-map-with a-map (current-output-port)))
    ((a-map port)
     (print-map-with a-map port))
    ))

;;; Creates a new vector of biotopes from given seed
(define (create-new-row seed width)
  (vector-unfold
   (lambda (j inner-seed)
     (let ((new-seed (mutate-biotope inner-seed)))
       (values new-seed new-seed)))
   width
   seed))

;;; Creates a map with given dimensions from an initial
;;; biotope seed.
(define (create-map seed-biotope width height)
  (let ((the-map
         (vector-unfold
          (lambda (i seed)
            (let ((new-row (create-new-row seed width)))
              (values
               new-row
               (vector-ref new-row (- width 1)))))
          height
          seed-biotope)))
    (map-map (lambda (bio) (list bio)) the-map)))

#;
(print-map (create-map '(#(1 1 1 1 1 0 0 0) . 10) 20 20))
