(import species
        (chicken io)
        (chicken sort)
        (chicken string))

(let* ((width (begin (print "Enter a width: ")
                       (read)))
       (height (begin (print "Enter a height: ")
                       (read)))
       (seed-biotope (begin (print "Enter a seed biotope: ")
                       (read)))
       (seed-genome (begin (print "Enter a seed genome: ")
                       (read)))
       (the-map (create-map seed-biotope width height))
       (ecosystem (make-ecosystem the-map)))

  (add-genome! the-map `(,(round  (/ width 2)) . ,(round  (/ height 2))) seed-genome)

  (let loop
      ((eco ecosystem))
    (begin
      (print "\x1b[2J")
      (print-map (eco 'biotopes))
      (print "Press [Enter] to evolve one step ")
      (read-line)
      (loop (evolve-ecosystem eco))
      )))
