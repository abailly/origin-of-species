(import scheme
        srfi-1
        generic-helpers
        (chicken sort))

;;; Merge a single pair into a list of pairs
(define (merge-one op pair assocs)
  (cond
   ((null? assocs) (list pair))
   (else
    (let ((head (car assocs)))
      (if (equal? (car head) (car pair))
          (cons (cons (car pair) (op (cdr pair) (cdr head)))
                (cdr assocs))
          (cons pair assocs))))))

;;; Merge a list of pairs by their keys using `op` to
;;; concatenate values
;;; `cmp` is a predicate for comparing two elems, which returns a negative, 0, or positive
;;; number if the first argument is less than, equal, or greater than the second.
(define (merge-assoc op cmp assocs)
  (let ((ordered (sort assocs
                       (lambda (a b) (< (cmp a b) 0)))))
    (fold ((curry merge-one) op)
          '() ordered)))
