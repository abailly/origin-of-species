(import generic-helpers)

(include "utils.scm")
(include "map.scm")

;; TODO use http://wiki.call-cc.org/eggref/5/srfi-60
;; ie. use bit-vectors instead of vectors of numbers

;;; 'Mutate' a single cell of given `genome`
;;; Utility function that simply inverts the given `gene` and returns
;;; the mutated vector
(define (mutate-genome-at! genome pos)
  (let* ((gene (vector-ref genome pos))
         (new-gene (case gene
                     ((1) 0)
                     ((0) 1))))
    (begin
      (vector-set! genome pos new-gene)
      genome)))

;;; Mutates given `genome`
;;; Returns a new mutated `genome`
(define (mutate genome)
  (let* ((len (vector-length genome))
         (mut (pseudo-random-integer len))
         (new-genome (vector-copy genome))
         )
    (cond
     ((< mut len)
      (mutate-genome-at! new-genome mut))
     (else genome))))


;;; Compute fitness of given genome for given biotope
;;; `genome` is a vector of bits (0 or 1)
;;; `biotope` is a vector of 3 different values: 0, 1, or `?`
;;; The fitness of a genome is equal to the number of genes that
;;; match the biotope's where `'?` matches any gene.
;;; Assumes `genome` and `biotope` have the same length.
;;; The rationale is that some biotopes are more stringent than othere, thus
;;; requiring better adaptation or conversely accepting less diverse genomes.
(define (fitness biotope genome)
  (let ((fit (lambda (match-count gene biome)
               (cond
                ((eq? '? biome) (+ match-count 1))
                ((eq? gene biome) (+ match-count 1))
                (else match-count))))
        (len (vector-length genome)))
    (/ (vector-fold fit 0 genome biotope) (+ 1 len))))

;;; Mutation rate, in per cent
(define-constant *mutation-rate* 1)

;;; Multiplier for each new generation
(define-constant *growth-factor* 2)

;;; Evolve a species over one generation
;;; Given a `genome` and a population size, this procedure yields next generation
;;; which is a list of one or more mutated genomes along with a new population
;;; size.
;;; Apparition of a new genome depends on the `*mutation-rate*` weighted by
;;; `population-size`
(define (next-generation gene population-size)
  (let* ((new-pop (* *growth-factor* population-size))
         (mut-pop (round (/  (* *mutation-rate* new-pop) 100))))
    (if (> mut-pop 0)
        (list (cons gene (-  new-pop mut-pop))
              (cons (mutate gene) mut-pop))
        (list (cons gene new-pop))
        )))


;;; Compute next generation of all genomes on given map
;;; `biotopes-map` is a vector of vectors of pairs where
;;; car is a biotope and cdr is a list of genomes.
;;; Returns a new map (vector of vectors) with each genome
;;; updated to next generation.
(define (next-generation-all biotopes-map)
  (map-map
   (lambda (biotope)
     (cons (car biotope)
           (concatenate
            (map (lambda (g)
                   (next-generation (car g) (cdr g)))
                 (cdr biotope)))))
   biotopes-map))

;;; Compute the population pressure on given biotope
(define (population-pressure biotope genomes)
  (/ (fold-right (lambda (genome pop)
                   (assert (and 'population-pressure (number? (cdr genome))))
                   (+ pop (cdr genome)))
                 0
                 genomes)
     (cdr biotope)))

;;; Computes some base scores of `genomes` in given `biotope`
;;;
;;; Return `values` of a list of fitness scores for each genome in the biotope,
;;; and the `pop-pressure` which is the ratio of total genomes population over
;;; the biotope's support value.
(define (base-scores biotope genomes)
  (let (;; fitness score for each genome
        (fitness-scores
         (map ((curry fitness) (car biotope)) (map car genomes)))
        ;; population pressure on the biotope, eg. the ratio of total population
        ;; over the biotope's support
        (pop-pressure (population-pressure biotope genomes)))
    (values
     fitness-scores
     pop-pressure)))

;;; Apply natural selection on a population of `genomes` in a given `biotope`
;;;
;;; The genomes' population size are reduced:
;;; * first, according to their fitness score within the biotope,
;;; * then according to the total population pressure within the biotope.
;;;
;;; `biotope` is a pair of a biome, a vector of ternary values, and a number limiting
;;; the population this biotope can support.
;;; `genomes` is a list of `(genome . population)` pairs
(define (selection biotope genomes)
  (let*-values (((fitness-scores pop-pressure) (base-scores biotope genomes))

                ;;; survival for each genome, based on their fitness score
                ;;; the population of each genome is simply multiplied by its fitness score
                ;;; which is always a value between 0 and 1
                ;;; This is a simplification, or a generalisation to populations of the concept
                ;;; of *survival of the fittest* which implies individuals which have a slight
                ;;; advantage over other individuals will have a higher survival rate.
                ((survival)
                 (map (lambda (gen fit)
                        (let* ((survivors (* (cdr gen) fit)))
                          (round  (if (> pop-pressure 1)
                                      (/ survivors pop-pressure)
                                      survivors))
                          ))
                      genomes
                      fitness-scores))

                )
    (map (lambda (gen surv)
           (cons (car gen) surv))
         genomes survival)))

;;; compares 2 genomes
;;; compares genomes using lexicographic ordering on the content of the
;;; `car` of the genomes
(define (compare-genome a b)
  (let* ((gene-a (car a))
         (gene-b (car b))
         (lena (vector-length gene-a))
         (lenb (vector-length gene-b)))
    (let loop ((index 0))
      (cond
       ;; check length of vectors
       ((and  (> lena index) (<= lenb index))
        1)
       ((and  (<= lena index) (> lenb index))
        -1)
       ((and  (= lena index) (= lenb index))
        0)
       ;; both vectors are lower than index, check content at ref
       (else
        (let ((the-a (vector-ref gene-a index))
              (the-b (vector-ref gene-b index)))
          (cond
           ((< the-a the-b)  -1)
           ((> the-a the-b)  1)
           ;; elements are equal, recurse
           (else (loop (+ 1 index))))
          ))))))

;;; Evolve the given list of `genomes` in the `biotope` for `count` generations
(define (evolve biotope genomes count)
  (let* ((massage-genomes (lambda (g) (merge-assoc + compare-genome g)))
         (next-gen
          (massage-genomes
           (concatenate (map (lambda (g)
                               (next-generation (car g) (cdr g)))
                             genomes))))
         (selected (filter (lambda (g) (not (= 0 (cdr g))))
                           (selection biotope next-gen))))
    (if (> count 0)
        (evolve biotope selected (- count 1))
        (massage-genomes selected))))

(define (make-ecosystem biotopes)
  (lambda (accessor . args)
    (case accessor
      ('biotopes biotopes)
      ('width (vector-length (vector-ref biotopes 0)))
      ('height (vector-length biotopes)))))

;;; update the `biotopes` at given `coordinates` adding the genome
;;; The `genome` is merged with existing genomes
(define (add-genome! biotopes coord genome)
  (let* ((row (vector-ref biotopes (car coord)))
         (biotope (vector-ref row (cdr coord)))
         (src-result (cons (car biotope)
                           (merge-assoc + compare-genome (cons genome (cdr biotope))))))
    (vector-set! row (cdr coord) src-result)
    biotopes
    ))

;;; decide whether or not given genome will try to migrate
;;; in an adjacent cell. This returns a procedure that given
;;; an accumulator and a genome, will possibly append some migration
;;; to the accumulator.
;;; the result of a migration is a list whose elements are:
;;; - the initial position of the genome `(i . j)`
;;; - the genome at this position, with negative migrated population
;;; - the migration destination `(i' . j')`
;;; - the migrating genome
(define (migrate-genome pressure width height i j)
  (lambda (genome acc)
    (if (< pressure 1)
        acc
        (let* ((inverse-pressure (/ 1 pressure)))
          (define (generate-migration acc)
            (let* ((migration-factor (/ 1 (+ 2 inverse-pressure)))  ; maximum is 50% of population
                   (migrating-pop (pseudo-random-integer
                                   (round (* (cdr genome) migration-factor))))
                   (migration-direction
                    (case (pseudo-random-integer 4)
                      ((0) (cons (modulo (- i 1) height) j))
                      ((1) (cons i (modulo  (+ j 1) width)))
                      ((2) (cons (modulo  (+ i 1) height) j))
                      ((3) (cons i (modulo  (- j 1) width)))
                      )))

              (if (> migrating-pop 0)
                  (cons (list
                         (cons i j)
                         (cons (car genome) (-  migrating-pop))
                         migration-direction
                         (cons (car genome) migrating-pop)
                         )
                        acc)
                  acc)))

          (if (> (pseudo-random-real) inverse-pressure)
              (generate-migration acc)
              acc)))))

;;; Compute migration for given `biotope` located at `(i,j)` on the map
;;; returns updated `migrations`
(define (migrate-biotope width height)
  (lambda (i j migrations biotope)
    (let* ((genomes (cdr biotope)))
      (if (not (null? genomes))
          (let  ((pressure (population-pressure (car biotope) genomes)))
            (fold (migrate-genome pressure width height i j) migrations genomes))
          migrations))))

(define (apply-migration! migration biotopes)
  (let*-values (((src gen-src dst gen-dst) (apply values migration)))
    (add-genome! biotopes src gen-src)
    (add-genome! biotopes dst gen-dst))
  biotopes)

;;; Compute migrations for each genome in `ecosystem`
;;; Migrations depend on the current population pressure for
;;; the biotope the genome occupies.
;;; Returns updated `biotopes`
(define (migrate-all! ecosystem)
  (let* ((width (ecosystem 'width))
         (height (ecosystem 'height))
         (biotopes (ecosystem 'biotopes))
         (migrations (fold-map-with-index
                      (migrate-biotope width height)
                      '()
                      biotopes)))

    (fold apply-migration! biotopes migrations)))

;;; Evolve the given ecosystem over given number of generation
;;; An ecosystem is made of 2 `width` x `height` matrices, one containing
;;; a list of `genomes` representing the local life-forms of each biotope,
;;; and the other one the map of  `biotopes`.
;;; It works in 3 phases:
;;;
;;; 1. Compute the `next-generation` for each biotope,
;;; 2. `migrate` species between adjacent biotopes when population pressure is
;;;    too high
;;; 3. Apply `selection` on each biotope.
(define (evolve-ecosystem ecosystem)
  (let* ((biotopes-map (ecosystem 'biotopes))
         (next-gen-ecosystem (make-ecosystem (next-generation-all biotopes-map))))

    (migrate-all! next-gen-ecosystem)

    (define (apply-selection biotope)
      (cons (car biotope)
            (selection (car biotope) (cdr biotope))))

    (let ((selected-biotopes (map-map apply-selection (next-gen-ecosystem 'biotopes))))
      (make-ecosystem selected-biotopes))))
