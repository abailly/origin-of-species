(import test)

(import species)

(define (cmp-integers a b)
  (- (car a) (car b)))

(test-group
 "Merge list of pairs"
 (test "merge an unsorted list of pairs" '((2 . 1) (1 . 5))
       (merge-assoc + cmp-integers '((1 . 2) (2 . 1) (1 . 3))))
 (test "merge an empty list of pairs" '()
       (merge-assoc + cmp-integers '()))
 )
