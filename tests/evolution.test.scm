(import test)

(import species)

;;; Utilities
(test-group
 "compare genomes"
 (test "greater yields 1" 1 (compare-genome '(#(1 0 0) .42) '(#(0 1 0) . 13)))
 (test "smaller yields -1" -1 (compare-genome '(#(0 1 0) .42) '(#(1 0 0) . 13)))
 (test "shortest is smaller" -1 (compare-genome '(#(0 1 0) .42) '(#(0 1 0 0) . 13)))
 (test "equals yield 0" 0 (compare-genome '(#(0 1 0) .42) '(#(0 1 0) . 13)))
 (test "empty vector are equals" 0 (compare-genome '(#() .42) '(#() . 13)))
 (test "empty vector is smaller" 1 (compare-genome '(#(1 0) .42) '(#() . 13)))
 )

(test-group
 "selection"
 (test-assert
     "fittest genomes have higher survival rate"
   (let ((selection-populations
          (map cdr
               (selection '(#(1 0 0) . 100)
                          '((#(1 0 0) . 10)
                            (#(0 0 0) . 10))))))

     (> (car selection-populations) (cadr selection-populations))))

 (test-assert
     "survival rate is always lower than 1 even with perfect fit"
   (let ((selection-populations
          (map cdr
               (selection '(#(1 0 0) . 100)
                          '((#(1 0 0) . 10))))))

     (< (car selection-populations) 10)))

 (test-assert
     "fitness results are unchanged given no population pressure"
   (let ((selection-populations
          (map cdr
               (selection '(#(1 0 0) . 100)
                          '((#(1 0 0) . 10))))))

     (< (car selection-populations) 10)))

 (test-assert
     "population is reduced if there is pressure"
   (let ((selection-populations
          (map cdr
               (selection '(#(1 0 0) . 100)
                          '((#(1 0 0) . 200))))))
     (print selection-populations)
     (< (car selection-populations) 100)))
 )

(define-constant empty-biotope
  #(
    #(((#(? ? ? 0) . 100)) ((#(? ? ? 1) . 100)))
    #(((#(? ? 1 ?) . 100)) ((#(? ? 0 ?) . 100)))
    ))

(test-group
 "add genome"
 (test "get biotope at coordinate"
       '((#(? ? ? 0) . 100))
       (biotope-at 0 0 empty-biotope))
 (test "get biotope at invalid coordinate"
       '()
       (biotope-at -1 12 empty-biotope))

 ;; this is bad, tests result depends on ordering of execution because we mutate
 ;; empty-biotope
 (test "add genome to empty biotope"
       '((#(? ? ? 0) . 100) (#(1 0 0 0) . 42))
       (biotope-at 0 0
                   (add-genome! empty-biotope '(0 . 0) '(#(1 0 0 0) . 42))))
 (test "add genome to non-empty biotope"
       '((#(? ? ? 0) . 100) (#(1 0 0 0) . 42) (#(0 1 0 0) . 43))
       (biotope-at 0 0
                   (add-genome! empty-biotope '(0 . 0) '(#(0 1 0 0) . 43))))
 (test "add genome to non-empty biotope sort by lexicographic order"
       '((#(? ? ? 0) . 100) (#(1 1 0 0) . 44) (#(1 0 0 0) . 42) (#(0 1 0 0) . 43))
       (biotope-at 0 0
                   (add-genome! empty-biotope '(0 . 0) '(#(1 1 0 0) . 44))))
 )
