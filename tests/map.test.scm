(import test)

(import species)

;;; Maps
(test-group
 "Map operations"
 (test "can map function over 1 map"
       #(#(2 4 6) #(4 6 8))
       (map-map (lambda (x) (* 2 x)) #(#(1 2 3) #(2 3 4))))
 (test "can map function over 2 maps"
       #(#(2 3 4) #(4 5 6))
       (map-map (lambda (x y) (+ y x))
                #(#(1 2 3) #(2 3 4))
                #(#(1 1 1) #(2 2 2))))
 (test "can map function with index over 1 map"
       (reverse '((0 . 0) (0 . 1) (0 . 2) (1 . 0) (1 . 1) (1 . 2)))
       (fold-map-with-index (lambda (i j acc x) (cons (cons i j) acc)) '() #(#(1 2 3) #(2 3 4))))
 )
