(module species (evolve evolve-ecosystem create-map add-genome! make-ecosystem
                        selection compare-genome merge-assoc map-map biotope-at
                        print-map
                        fold-map-with-index)
(import (chicken base))

(include "map.scm")
(include "utils.scm")
(include "evolution.scm")

)
