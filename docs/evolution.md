#### <a id="def-mutate-genome-at!"></a> [procedure] `(mutate-genome-at! GENOME POS)`
  
'Mutate' a single cell of given `genome`
Utility function that simply inverts the given `gene` and returns
the mutated vector  

#### <a id="def-mutate"></a> [procedure] `(mutate GENOME)`
  
Mutates given `genome`
Returns a new mutated `genome`  

#### <a id="def-fitness"></a> [procedure] `(fitness BIOTOPE GENOME)`
  
Compute fitness of given genome for given biotope
`genome` is a vector of bits (0 or 1)
`biotope` is a vector of 3 different values: 0, 1, or `?`
The fitness of a genome is equal to the number of genes that
match the biotope's where `'?` matches any gene.
Assumes `genome` and `biotope` have the same length.
The rationale is that some biotopes are more stringent than othere, thus
requiring better adaptation or conversely accepting less diverse genomes.  

#### <a id="def-*mutation-rate*"></a> [constant] `*mutation-rate*`  
**value:** `1`  
Mutation rate, in per cent  

#### <a id="def-*growth-factor*"></a> [constant] `*growth-factor*`  
**value:** `2`  
Multiplier for each new generation  

#### <a id="def-next-generation"></a> [procedure] `(next-generation GENOME POPULATION-SIZE)`
  
Evolve a species over one generation
Given a `genome` and a population size, this procedure yields next generation
which is a list of one or more mutated genomes along with a new population
size.
Apparition of a new genome depends on the `*mutation-rate*` weighted by
`population-size`  

#### <a id="def-base-scores"></a> [procedure] `(base-scores BIOTOPE GENOMES)`
  
Computes some base scores of `genomes` in given `biotope`

Return `values` of a list of fitness scores for each genome in the biotope,
and the `pop-pressure` which is the ratio of total genomes population over
the biotope's support value.  

#### <a id="def-selection"></a> [procedure] `(selection BIOTOPE GENOMES)`
  
Apply natural selection on a population of `genomes` in a given `biotope`

The genomes' population size are reduced:
* first, according to their fitness score within the biotope,
* then according to the total population pressure within the biotope.

`biotope` is a pair of a biome, a vector of ternary values, and a number limiting
the population this biotope can support.
`genomes` is a list of `(genome . population)` pairs  

#### <a id="def-compare-genome"></a> [procedure] `(compare-genome A B)`
  
compares 2 genomes
compares genomes using lexicographic ordering on the content of the
`car` of the genomes  

#### <a id="def-evolve"></a> [procedure] `(evolve BIOTOPE GENOMES COUNT)`
  
Evolve the given list of `genomes` in the `biotope` for `count` generations  

