# The Origin of Species

A simple model in [Chicken Scheme](http://call-cc.org) of Darwin's theory of _natural selection_ as exposed in his famous book [On the origin of Species](https://en.wikipedia.org/wiki/On_the_Origin_of_Species).

## Why?

There is not a dearth of computerised or mathematical modeling of Darwin's theory. And of course evoluationary algorithms are directly inspired by those theories. [Darwin's book](https://global.oup.com/academic/product/on-the-origin-of-species-9780199219223) is a great read: He meticulously and precisely, yet without overloading the reader's brain with scientific jargon, explains, motivates, and demonstrates how species evolved. While reading it, being the nerd I am, I kept thinking it would be an interesting exercise to encode the main concepts of Darwin, namely _Natural Selection_ or _Survival of the Fittest_, in a computer program.

I viewed this as an opportunity to revive my long extinct interest in more dynamic functional programming languages than Haskell. I have always been fascinated by Scheme's elegance and concision, qualities that reached their peak in the [R5RS](https://schemers.org/Documents/Standards/R5RS/r5rs.pdf) which describes the language, including its formal semantics, in exactly 50 pages. I recently bought and skimmed over Sussman's [Software Design for Flexibility](https://www.amazon.fr/Software-Design-Flexibility-Programming-Yourself/dp/0262045494) and while I did not really liked the book, it also aroused again my interest in Scheme.

## Install

Needs a working Chicken scheme installation.
In the source directory, run

```
$ CHICKEN_REPOSITORY_PATH=$(pwd):$(chicken-install -repository) chicken-install -test -n
```

Setting `CHICKEN_REPOSITORY_PATH` allows tests to find the locally compiled component library.

This should yield some test output and result in having a `darwin` executable file in the current directory.

## Usage

Here is an example run of `darwin`:

```
$ ./darwin
Enter a width:
> 30
Enter a height:
> 20
Enter a seed biotope:
> (#(0 0 0 0 0 0 0 0) . 1000)
Enter a seed genome:
> (#(0 0 0 0 0 0 0 1) . 1000)
```

Then it continuously evolves the map, displaying each step using some color coding of the terrain and the number of species in each cell:

![](./sample-ecosystem.png)

* The *biotope* is a pair made of a vector *trits* (eg. _ternary digits_, either `1`, `0` or `?`), and a maximum supported population
* The *genome* is a pair of a vector of bits and a population.
* Both vectors must have the same length but the length is free.

## Code

* See [evolution](./docs/evolution.md) for some documentation (experiment with [scm2wiki](http://wiki.call-cc.org/eggref/5/scm2wiki))
